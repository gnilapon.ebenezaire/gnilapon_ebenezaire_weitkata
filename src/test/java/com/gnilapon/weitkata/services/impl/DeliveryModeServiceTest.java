package com.gnilapon.weitkata.services.impl;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.DeliveryModeReq;
import com.gnilapon.weitkata.models.dto.responses.DeliveryModeResp;
import com.gnilapon.weitkata.models.entities.DeliveryMode;
import com.gnilapon.weitkata.models.entities.Users;
import com.gnilapon.weitkata.models.entities.UsersMode;
import com.gnilapon.weitkata.models.enums.Mode;
import com.gnilapon.weitkata.repositories.DeliveryModeRepository;
import com.gnilapon.weitkata.repositories.UsersRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DeliveryModeServiceTest {

    @Mock
    private UsersRepository usersRepository;

    @Mock
    private DeliveryModeRepository deliveryModeRepository;

    @InjectMocks
    private DeliveryModeService deliveryModeService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetDeliveryModeByMode() {
        // Mocking the repository response
        when(deliveryModeRepository.findByMode(any(Mode.class))).thenReturn(someDeliveryModeEntity());

        try {
            DeliveryModeResp result = deliveryModeService.getDeliveryModeByMode(Mode.DELIVERY);
            assertNotNull(result);
            //assertEquals("Delivery", result.getMode());
        } catch (WeItKataException e) {
            fail("Exception not expected: " + e.getMessage());
        }
    }

    @Test
    void testCreateDeliveryMode() {
        // Mocking the repository response
        when(deliveryModeRepository.findByMode(any(Mode.class))).thenReturn(null);
        when(deliveryModeRepository.save(any(DeliveryMode.class))).thenReturn(someDeliveryModeEntity());

        DeliveryModeReq deliveryModeReq = new DeliveryModeReq();
        deliveryModeReq.setMode(Mode.DELIVERY);

        try {
            DeliveryModeResp result = deliveryModeService.createDeliveryMode(deliveryModeReq);
            assertNotNull(result);
            //assertEquals("Delivery", result.getMode());
        } catch (WeItKataException e) {
            fail("Exception not expected: " + e.getMessage());
        }
    }

    @Test
    void testDeleteDeliveryMode() {
        // Mocking the repository response
        when(deliveryModeRepository.findById("1")).thenReturn(Optional.of(someDeliveryModeEntity()));

        try {
            deliveryModeService.deleteDeliveryMode("1");
            // Verify that delete method is called
            verify(deliveryModeRepository, times(1)).delete(any(DeliveryMode.class));
        } catch (WeItKataException e) {
            fail("Exception not expected: " + e.getMessage());
        }
    }

    // Helper methods to create mock entities

    private List<Users> someUserEntity() {
        Users user = new Users();
        user.setId("1");
        user.setFullName("John");
        user.setUsersModes(Collections.singletonList(someUserModeEntity()));
        return Collections.singletonList(user);
    }

    private UsersMode someUserModeEntity() {
        UsersMode userMode = new UsersMode();
        userMode.setId("1");
        userMode.setUsers(someUserEntity().get(0));
        userMode.setDeliveryMode(someDeliveryModeEntity());
        return userMode;
    }

    private DeliveryMode someDeliveryModeEntity() {
        DeliveryMode deliveryMode = new DeliveryMode();
        deliveryMode.setId("1");
        deliveryMode.setMode(Mode.DELIVERY);
        return deliveryMode;
    }
}
