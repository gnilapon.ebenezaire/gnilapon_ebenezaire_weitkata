package com.gnilapon.weitkata.services;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.DeliveryModeReq;
import com.gnilapon.weitkata.models.dto.responses.DeliveryModeResp;
import com.gnilapon.weitkata.models.enums.Mode;

import java.util.List;

public interface IDeliveryMode {
    /**
     * get deliveries modes by client full name
     * @param fullName
     * @return List<DeliveryModeResp>
     * @throws WeItKataException
     */
    List<DeliveryModeResp> getDeliveryModeByClientFullName(String fullName) throws WeItKataException;/**


     * get deliveries modes by client Id
     * @param usersI
     * @return List<DeliveryModeResp>
     * @throws WeItKataException
     */
    List<DeliveryModeResp> getDeliveryModeByClientId(String usersI) throws WeItKataException;

    /**
     * get delivery mode by mode
     * @param mode
     * @return DeliveryModeResp
     * @throws WeItKataException
     */
    DeliveryModeResp getDeliveryModeByMode(Mode mode) throws WeItKataException;

    /**
     * Create delivery mode
     * @param modeReq
     * @return DeliveryModeResp
     * @throws WeItKataException
     */
    DeliveryModeResp createDeliveryMode(DeliveryModeReq modeReq) throws WeItKataException;/**

     * Delete delivery mode
     * @param deliveryModeId
     * @throws WeItKataException
     */
    void deleteDeliveryMode(String deliveryModeId) throws WeItKataException;
}
