package com.gnilapon.weitkata.services;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.UserModeReq;
import com.gnilapon.weitkata.models.dto.responses.UsersModeResp;

public interface IUsersMode {
    /**
     * Create users mode
     * @param userModeReq
     * @return UsersModeResp
     * @throws WeItKataException
     */
    UsersModeResp createUserMode(UserModeReq userModeReq) throws WeItKataException;
}
