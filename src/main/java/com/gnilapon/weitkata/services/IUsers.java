package com.gnilapon.weitkata.services;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.UsersLogin;
import com.gnilapon.weitkata.models.dto.requests.UsersReq;
import com.gnilapon.weitkata.models.dto.responses.UsersResp;

public interface IUsers {
    /**
     * Search users by full name
     * @param fullName
     * @return UsersResp
     * @throws WeItKataException
     */
    UsersResp findByFullNameContaining(String fullName) throws WeItKataException;

    /**
     * Create users
     * @param req
     * @return UsersResp
     * @throws WeItKataException
     */
    UsersResp create(UsersReq req) throws WeItKataException;

    /**
     * Login users
     * @param usersLogin
     * @return UsersResp
     * @throws WeItKataException
     */
    UsersResp login(UsersLogin usersLogin) throws WeItKataException;

}
