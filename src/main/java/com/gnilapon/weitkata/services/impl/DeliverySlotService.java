package com.gnilapon.weitkata.services.impl;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.mappers.DeliverySlotMapper;
import com.gnilapon.weitkata.models.dto.requests.DeliverySlotReq;
import com.gnilapon.weitkata.models.dto.responses.DeliverySlotResp;
import com.gnilapon.weitkata.models.entities.DeliverySlot;
import com.gnilapon.weitkata.models.enums.Mode;
import com.gnilapon.weitkata.repositories.DeliveryModeRepository;
import com.gnilapon.weitkata.repositories.DeliverySlotRepository;
import com.gnilapon.weitkata.services.IDeliverySlot;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
@Slf4j
@Transactional
public class DeliverySlotService implements IDeliverySlot {

    private DeliveryModeRepository deliveryModeRepository;
    private DeliverySlotRepository deliverySlotRepository;

    /**
     * Get deliveries slots by delivery mode
     *
     * @param mode
     * @return List<DeliverySlotResp>
     * @throws WeItKataException
     */
    @Override
    public List<DeliverySlotResp> getDeliverySlotsByDeliveryMode(Mode mode) throws WeItKataException {

        try {
            log.error("get deliveries slots by delivery mode :  {}",mode.getModeName());
            var resp = deliveryModeRepository.findByMode(mode);
            if (Objects.nonNull(resp) && Objects.nonNull(resp.getDeliverySlots())){
                return resp.getDeliverySlots().stream().map(DeliverySlotMapper.INSTANCE::toDeliverySlotResp).collect(Collectors.toList());
            }
            return Collections.emptyList();
        } catch (RuntimeException e){
            log.error("get deliveries slots by delivery mode not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }

    /**
     * Create delivery slot
     *
     * @param slotReq
     * @return DeliverySlotResp
     * @throws WeItKataException
     */
    @Override
    public DeliverySlotResp createDeliverySlots(DeliverySlotReq slotReq) throws WeItKataException {
        try {
            var mode = deliveryModeRepository.findById(slotReq.getDeliveryModeId());
            if (mode.isPresent()){
                log.error("Delivery slot was create successfully :  {}", slotReq);
                var slot = new DeliverySlot();
                slot.setEndTime(slotReq.getEndTime());
                slot.setStartTime(slotReq.getStartTime());
                slot.setWeekDays(slotReq.getWeekDays());
                slot.setDeliveryMode(mode.get());
                slot.setCreateDate(new Date());
                slot.setUpdateDate(new Date());
                return DeliverySlotMapper.INSTANCE.toDeliverySlotResp(deliverySlotRepository.save(slot));
            } else {
                log.error("Create delivery slot not work because mode by Id {} don't exist",slotReq.getDeliveryModeId());
                throw new WeItKataException("Create delivery slot not work because mode by Id "+slotReq.getDeliveryModeId()+" don't exist");
            }
        }catch (RuntimeException e){
            log.error("Create delivery slot by mode not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }
}
