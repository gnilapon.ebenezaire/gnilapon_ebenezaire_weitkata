package com.gnilapon.weitkata.services.impl;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.mappers.UsersMapper;
import com.gnilapon.weitkata.models.dto.requests.UsersLogin;
import com.gnilapon.weitkata.models.dto.requests.UsersReq;
import com.gnilapon.weitkata.models.dto.responses.UsersResp;
import com.gnilapon.weitkata.repositories.UsersRepository;
import com.gnilapon.weitkata.services.IUsers;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.Date;
import java.util.Objects;

@AllArgsConstructor
@Service
@Slf4j
@Transactional
public class UsersService implements IUsers {

    private UsersRepository usersRepository;

    /**
     * Search users by full name
     *
     * @param fullName
     * @return UsersResp
     * @throws WeItKataException
     */
    @Override
    public UsersResp findByFullNameContaining(String fullName) throws WeItKataException{
        try {
            if (Objects.nonNull(fullName)){
                log.info("search users by full name :  {}",fullName);
                return UsersMapper.INSTANCE.toUsersResp(usersRepository.findByFullNameContaining(fullName)) ;
            }else {
                log.error("search users by full name not work because fullName is null");
                throw new WeItKataException("search users by full name not work because fullName is null");
            }
        } catch (RuntimeException e){
            log.error("search users by full name not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }

    /**
     * Create users
     *
     * @param usersReq
     * @return UsersResp
     * @throws WeItKataException
     */
    @Override
    public UsersResp create(UsersReq usersReq) throws WeItKataException {
        try {
            var userE = usersRepository.findByEmail(usersReq.getEmail());
            if (Objects.nonNull(userE)){
                log.error("Create user not work because {} already exist",usersReq.getEmail());
                throw new WeItKataException("Create user not work because "+usersReq.getEmail()+" already exist");
            }else {
                log.info("User successfully create {}",usersReq.getEmail());
                var user = UsersMapper.INSTANCE.toUsers(usersReq);
                user.setCreateDate(new Date());
                user.setUpdateDate(new Date());
                return UsersMapper.INSTANCE.toUsersResp(usersRepository.save(user));
            }
        } catch (RuntimeException e){
            log.error("create users not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }

    /**
     * Login users
     *
     * @param usersLogin
     * @return
     * @throws WeItKataException
     */
    @Override
    public UsersResp login(UsersLogin usersLogin) throws WeItKataException{
        try {
            var user = usersRepository.findByEmail(usersLogin.getEmail());
            if (Objects.nonNull(user) && user.getPassword().equals(usersLogin.getPassword())){
                log.info("user successfully login {}",usersLogin.getEmail());
                return UsersMapper.INSTANCE.toUsersResp(user);
            }else {
                log.warn("password or email incorrect for {}", usersLogin.getEmail());
                throw new WeItKataException("password or email incorrect for "+usersLogin.getEmail());
            }
        } catch (RuntimeException e){
            log.warn("login failure because {} ", e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }
}
