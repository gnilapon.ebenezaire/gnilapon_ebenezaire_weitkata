package com.gnilapon.weitkata.services.impl;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.mappers.UserModeMapper;
import com.gnilapon.weitkata.models.dto.requests.UserModeReq;
import com.gnilapon.weitkata.models.dto.responses.UsersModeResp;
import com.gnilapon.weitkata.models.entities.UsersMode;
import com.gnilapon.weitkata.repositories.DeliveryModeRepository;
import com.gnilapon.weitkata.repositories.UsersModeRepository;
import com.gnilapon.weitkata.repositories.UsersRepository;
import com.gnilapon.weitkata.services.IUsersMode;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@AllArgsConstructor
@Service
@Slf4j
@Transactional
public class UsersModeService implements IUsersMode {

    private UsersRepository usersRepository;
    private DeliveryModeRepository deliveryModeRepository;
    private UsersModeRepository usersModeRepository;

    /**
     * Create users mode
     *
     * @param userModeReq
     * @return UsersModeResp
     * @throws WeItKataException
     */
    @Override
    public UsersModeResp createUserMode(UserModeReq userModeReq) throws WeItKataException {

        try {
            var user = usersRepository.findById(userModeReq.getUserId());
            var mode = deliveryModeRepository.findById(userModeReq.getDeliveryModeId());
            if (mode.isPresent() && user.isPresent()){
                log.error("Users Mode  was create successfully :  {}", userModeReq);
                var userMode = new UsersMode();
                userMode.setDeliveryMode(mode.get());
                userMode.setUsers(user.get());
                userMode.setCreateDate(new Date());
                userMode.setUpdateDate(new Date());
                return UserModeMapper.INSTANCE.toUserModeResp(usersModeRepository.save(userMode));
            } else {
                log.error("Create Users Mode not work because users by Id {} or mode by Id {}  don't exist",userModeReq.getUserId(),userModeReq.getDeliveryModeId());
                throw new WeItKataException("Create delivery slot not work because users by Id "+userModeReq.getUserId()+" or mode by Id "+userModeReq.getDeliveryModeId()+" don't exist");
            }
        }catch (RuntimeException e){
            log.error("Create Users Mode by mode not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }
}
