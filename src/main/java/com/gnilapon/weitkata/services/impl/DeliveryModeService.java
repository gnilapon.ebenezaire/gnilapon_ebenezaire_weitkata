package com.gnilapon.weitkata.services.impl;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.mappers.DeliveryModeMapper;
import com.gnilapon.weitkata.models.dto.requests.DeliveryModeReq;
import com.gnilapon.weitkata.models.dto.responses.DeliveryModeResp;
import com.gnilapon.weitkata.models.enums.Mode;
import com.gnilapon.weitkata.repositories.DeliveryModeRepository;
import com.gnilapon.weitkata.repositories.UsersRepository;
import com.gnilapon.weitkata.services.IDeliveryMode;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
@Slf4j
@Transactional
public class DeliveryModeService implements IDeliveryMode {
    private UsersRepository usersRepository;
    private DeliveryModeRepository deliveryModeRepository;
    /**
     * Get deliveries modes by client full name
     *
     * @param fullName
     * @return List<DeliveryModeResp>
     * @throws WeItKataException
     */
    @Override
    public List<DeliveryModeResp> getDeliveryModeByClientFullName(String fullName) throws WeItKataException {

        try {

            if (Objects.nonNull(fullName)){
                log.error("get deliveries modes by client full name :  {}",fullName);
                var resp = usersRepository.findByFullNameContaining(fullName);
                if (Objects.nonNull(resp) && Objects.nonNull(resp.getUsersModes())){
                    return resp.getUsersModes().stream().filter(x->x.getUsers().getId().equals(resp.getId())).map(x->DeliveryModeMapper.INSTANCE.toDeliveryModeResp(x.getDeliveryMode())).collect(Collectors.toList());
                }
                return Collections.emptyList();
            }else {
                log.error("get deliveries modes by full name not work because fullName is null");
                throw new WeItKataException("get deliveries modes by full name not work because fullName is null");
            }
        } catch (RuntimeException e){
            log.error("get deliveries modes by client full name not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }

    /**
     * get deliveries modes by client Id
     *
     * @param usersI
     * @return List<DeliveryModeResp>
     * @throws WeItKataException
     */
    @Override
    public List<DeliveryModeResp> getDeliveryModeByClientId(String usersI) throws WeItKataException {
        try {

            if (Objects.nonNull(usersI)){
                log.error("get deliveries modes by client Id :  {}",usersI);
                var resp = usersRepository.findById(usersI);
                if (resp.isPresent() && Objects.nonNull(resp.get().getUsersModes())){
                    return resp.get().getUsersModes().stream().filter(x->x.getUsers().getId().equals(resp.get().getId())).map(x->DeliveryModeMapper.INSTANCE.toDeliveryModeResp(x.getDeliveryMode())).collect(Collectors.toList());
                }
                return Collections.emptyList();
            }else {
                log.error("get deliveries modes by Id not work because Id is null");
                throw new WeItKataException("get deliveries modes by Id not work because Id is null");
            }
        } catch (RuntimeException e){
            log.error("get deliveries modes by client Id not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }

    /**
     * Get delivery mode by mode
     *
     * @param mode
     * @return DeliveryModeResp
     * @throws WeItKataException
     */
    @Override
    public DeliveryModeResp getDeliveryModeByMode(Mode mode) throws WeItKataException {
        try {
            log.error("get delivery mode by mode :  {}",mode.getModeName());
            return DeliveryModeMapper.INSTANCE.toDeliveryModeResp(deliveryModeRepository.findByMode(mode)) ;
        } catch (RuntimeException e){
            log.error("get delivery mode by mode not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }

    /**
     * Create delivery mode
     *
     * @param modeReq
     * @return DeliveryModeResp
     * @throws WeItKataException
     */
    @Override
    public DeliveryModeResp createDeliveryMode(DeliveryModeReq modeReq) throws WeItKataException {
        try {
            var del = deliveryModeRepository.findByMode(modeReq.getMode());
            if (Objects.nonNull(del)){
                log.error("Create delivery mode not work because {} already exist",modeReq.getMode());
                throw new WeItKataException("Create delivery mode not work because "+modeReq.getMode()+" already exist");
            }
            log.error("Delivery mode was create successfully :  {}", modeReq);
            var mode = DeliveryModeMapper.INSTANCE.toDeliveryMode(modeReq);
            mode.setCreateDate(new Date());
            mode.setUpdateDate(new Date());
            return  DeliveryModeMapper.INSTANCE.toDeliveryModeResp(deliveryModeRepository.save(mode));
        } catch (RuntimeException e){
            log.error("Create delivery mode not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }

    /**
     * Delete delivery mode
     *
     * @param deliveryModeId
     * @throws WeItKataException
     */
    @Override
    public void deleteDeliveryMode(String deliveryModeId) throws WeItKataException {
        try {
            var del = deliveryModeRepository.findById(deliveryModeId);
            if (del.isEmpty()){
                log.error("Delete delivery mode by mode not work because {} don't exist",deliveryModeId);
                throw new WeItKataException("Delete delivery mode by mode not work because "+deliveryModeId+" don't exist");
            }
            log.error("Delivery mode was delete successfully :  {}", del.get().getMode().getModeName());
            deliveryModeRepository.delete(del.get());
        } catch (RuntimeException e){
            log.error("Delete delivery mode by mode not work because {}",e.getMessage());
            throw new WeItKataException(e.getMessage(),e);
        }
    }
}
