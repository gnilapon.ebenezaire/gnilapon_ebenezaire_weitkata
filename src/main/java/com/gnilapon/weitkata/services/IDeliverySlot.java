package com.gnilapon.weitkata.services;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.DeliverySlotReq;
import com.gnilapon.weitkata.models.dto.responses.DeliverySlotResp;
import com.gnilapon.weitkata.models.enums.Mode;

import java.util.List;

public interface IDeliverySlot {
    /**
     * get deliveries slots by delivery mode
     * @param mode
     * @return List<DeliverySlotResp>
     * @throws WeItKataException
     */
    List<DeliverySlotResp> getDeliverySlotsByDeliveryMode(Mode mode) throws WeItKataException;

    /**
     * Create delivery slot
     * @param slotReq
     * @return DeliverySlotResp
     * @throws WeItKataException
     */
    DeliverySlotResp createDeliverySlots(DeliverySlotReq slotReq) throws WeItKataException;
}
