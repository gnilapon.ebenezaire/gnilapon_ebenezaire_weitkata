package com.gnilapon.weitkata.models.entities;

import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Getter
@Setter
public abstract class Common {
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @CreatedDate
    private Date createDate;
    @Temporal (TemporalType.TIMESTAMP)
    @DateTimeFormat (pattern = "yyyy-MM-dd hh:mm:ss")
    @CreatedDate
    private Date updateDate;
}
