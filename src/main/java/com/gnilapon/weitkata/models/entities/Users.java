package com.gnilapon.weitkata.models.entities;

import com.gnilapon.weitkata.models.enums.EnumRole;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Users implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(columnDefinition = "VARCHAR(36)")
    private String id;
    private String fullName;
    private String password;
    private String email;
    @Enumerated(EnumType.STRING)
    private EnumRole role;
    @OneToMany(targetEntity = UsersMode.class, mappedBy = "users", fetch = FetchType.LAZY)
    private List<UsersMode> usersModes = new ArrayList<>();
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @CreatedDate
    private Date createDate;
    @Temporal (TemporalType.TIMESTAMP)
    @DateTimeFormat (pattern = "yyyy-MM-dd hh:mm:ss")
    @CreatedDate
    private Date updateDate;
}
