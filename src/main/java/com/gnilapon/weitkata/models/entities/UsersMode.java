package com.gnilapon.weitkata.models.entities;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UsersMode extends Common implements Serializable {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(columnDefinition = "VARCHAR(36)")
    private String id;
    @ManyToOne(targetEntity = Users.class, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "user_id")
    @ToString.Exclude
    private Users users;
    @ManyToOne(targetEntity = DeliveryMode.class, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, name = "delivery_mode_id")
    @ToString.Exclude
    private DeliveryMode deliveryMode;
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    @CreatedDate
    private Date createDate;
    @Temporal (TemporalType.TIMESTAMP)
    @DateTimeFormat (pattern = "yyyy-MM-dd hh:mm:ss")
    @CreatedDate
    private Date updateDate;
}
