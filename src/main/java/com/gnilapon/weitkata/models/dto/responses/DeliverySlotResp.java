package com.gnilapon.weitkata.models.dto.responses;

import com.gnilapon.weitkata.models.enums.WeekDays;
import lombok.*;

import java.time.LocalTime;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DeliverySlotResp{
    private String id;
    private LocalTime startTime;
    private LocalTime endTime;
    private Date createDate;
    private Date updateDate;
    private WeekDays weekDays;
    private DeliveryModeResp deliveryMode;
}
