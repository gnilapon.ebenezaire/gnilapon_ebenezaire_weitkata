package com.gnilapon.weitkata.models.dto.requests;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UsersLogin {
    @NotBlank(message = "password can not be blank")
    @NotNull(message = "password can not be null ")
    private String password;
    @NotBlank(message = "email can not be blank")
    @NotNull(message = "email can not be null ")
    private String email;
}
