package com.gnilapon.weitkata.models.dto.requests;

import com.gnilapon.weitkata.models.enums.WeekDays;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.LocalTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DeliverySlotReq {
    @NotBlank(message = "weekDays mode can not be blank")
    @NotNull(message = "weekDays mode can not be null ")
    private WeekDays weekDays;
    @NotBlank(message = "startTime mode can not be blank")
    @NotNull(message = "startTime mode can not be null ")
    private LocalTime startTime;
    @NotBlank(message = "endTime mode can not be blank")
    @NotNull(message = "endTime mode can not be null ")
    private LocalTime endTime;
    @NotBlank(message = "deliveryModeId mode can not be blank")
    @NotNull(message = "deliveryModeId mode can not be null ")
    private String deliveryModeId;
}
