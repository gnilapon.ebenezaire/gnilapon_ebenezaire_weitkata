package com.gnilapon.weitkata.models.dto.requests;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserModeReq {
    @NotBlank(message = "userId can not be blank")
    @NotNull(message = "userId can not be null ")
    private String userId;
    @NotBlank(message = "deliveryModeId can not be blank")
    @NotNull(message = "deliveryModeId can not be null ")
    private String deliveryModeId;
}
