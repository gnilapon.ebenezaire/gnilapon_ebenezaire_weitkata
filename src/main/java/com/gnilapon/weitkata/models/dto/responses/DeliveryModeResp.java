package com.gnilapon.weitkata.models.dto.responses;

import com.gnilapon.weitkata.models.enums.Mode;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryModeResp{
    private String id;
    private Mode mode;
    private String description;
    private Date createDate;
    private Date updateDate;
}
