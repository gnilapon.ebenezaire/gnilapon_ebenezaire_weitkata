package com.gnilapon.weitkata.models.dto.requests;

import com.gnilapon.weitkata.models.enums.EnumRole;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UsersReq {
    private String fullName;
    @NotBlank(message = "password mode can not be blank")
    @NotNull(message = "password can not be null ")
    private String password;
    @NotBlank(message = "email can not be blank")
    @NotNull(message = "email can not be null ")
    private String email;
    @NotBlank(message = "role can not be blank")
    @NotNull(message = "role can not be null ")
    private EnumRole role = EnumRole.USER;
}
