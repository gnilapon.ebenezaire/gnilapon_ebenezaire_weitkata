package com.gnilapon.weitkata.models.dto.responses;

import com.gnilapon.weitkata.models.entities.Common;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UsersModeResp{
    private UsersResp users;
    private DeliveryModeResp deliveryMode;
    private Date createDate;
    private Date updateDate;
}
