package com.gnilapon.weitkata.models.dto.requests;

import com.gnilapon.weitkata.models.enums.Mode;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryModeReq {
    @NotBlank(message = "mode can not be blank")
    @NotNull(message = "mode can not be null ")
    private Mode mode ;
    private String description;
}
