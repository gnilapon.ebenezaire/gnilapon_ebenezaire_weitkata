package com.gnilapon.weitkata.models.dto.responses;

import com.gnilapon.weitkata.models.enums.EnumRole;
import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UsersResp{
    private String id;
    private String fullName;
    private String password;
    private String email;
    private EnumRole role;
    private Date createDate;
    private Date updateDate;
}
