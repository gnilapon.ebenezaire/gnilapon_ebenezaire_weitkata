package com.gnilapon.weitkata.models.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EnumRole {
    ADMIN("ADMIN"),
    USER("USER"),
    CLIENT("CLIENT");
    private final String roleName;
}
