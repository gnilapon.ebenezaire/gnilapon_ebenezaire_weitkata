package com.gnilapon.weitkata.exceptions;

public class WeItKataException extends RuntimeException{
    public WeItKataException(String message) {
        super(message);
    }

    public WeItKataException(String message, Throwable cause) {
        super(message,cause);
    }
}
