package com.gnilapon.weitkata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = "com.gnilapon.weitkata")
public class WeItKataApplication {

	public static void main(String[] args) {
		SpringApplication.run(WeItKataApplication.class, args);
	}

}
