package com.gnilapon.weitkata.web;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.UsersLogin;
import com.gnilapon.weitkata.models.dto.requests.UsersReq;
import com.gnilapon.weitkata.models.dto.responses.ApiErrorResponse;
import com.gnilapon.weitkata.models.dto.responses.UsersResp;
import com.gnilapon.weitkata.services.IUsers;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Users", description = "The users api")
@RestController
@AllArgsConstructor
@RequestMapping("/users")
@CrossOrigin(origins = "*")
@Slf4j
public class UsersController {

    private IUsers usersService;

    @Operation(
            summary = "Create users",
            description = "Create users entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "Save users",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = UsersResp.class))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping("/add")
    public ResponseEntity<?> create(@Valid @RequestBody UsersReq req) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(usersService.create(req));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }

    @Operation(
            summary = "Login users",
            description = "Login users entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "202",description = "Login users",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = UsersResp.class))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody UsersLogin req) {
        try {
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(usersService.login(req));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }
}
