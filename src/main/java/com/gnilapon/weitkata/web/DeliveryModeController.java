package com.gnilapon.weitkata.web;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.DeliveryModeReq;
import com.gnilapon.weitkata.models.dto.responses.ApiErrorResponse;
import com.gnilapon.weitkata.models.dto.responses.DeliveryModeResp;
import com.gnilapon.weitkata.models.enums.Mode;
import com.gnilapon.weitkata.services.IDeliveryMode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Delivery mode", description = "The delivery mode api")
@RestController
@AllArgsConstructor
@RequestMapping("/delivery-mode")
@CrossOrigin(origins = "*")
@Slf4j
public class DeliveryModeController {
    private IDeliveryMode iDeliveryMode;

    @Operation(
            summary = "Create delivery mode",
            description = "Create delivery mode entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "Save delivery mode",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = DeliveryModeResp.class))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping("/add")
    public ResponseEntity<?> create(@Valid @RequestBody DeliveryModeReq req) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(iDeliveryMode.createDeliveryMode(req));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }

    @Operation(
            summary = "Get deliveries modes for client",
            description = "Get deliveries modes by client full name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Get deliveries modes",content = {@Content(mediaType = "application/json",array = @ArraySchema(schema = @Schema(implementation = DeliveryModeResp.class)))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("/client")
    public ResponseEntity<?> getDeliveryModeByClientFullName(@Valid @RequestParam(name = "fname",required = false) String fullName) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(iDeliveryMode.getDeliveryModeByClientFullName(fullName));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }

    @Operation(
            summary = "Get deliveries modes by client Id",
            description = "Get deliveries modes by client Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Get deliveries modes by client Id",content = {@Content(mediaType = "application/json",array = @ArraySchema(schema = @Schema(implementation = DeliveryModeResp.class)))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{usersId}/client")
    public ResponseEntity<?> getDeliveryModeByClientId(@Valid @PathVariable(name = "usersId",required = false) String usersId) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(iDeliveryMode.getDeliveryModeByClientId(usersId));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }

    @Operation(
            summary = "Get deliveries modes by mode",
            description = "Get deliveries modes by mode")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Get deliveries modes by mode",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = DeliveryModeResp.class))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity<?> getDeliveryModeByMode(@Valid @RequestParam(name = "mode" , required = false) Mode mode) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(iDeliveryMode.getDeliveryModeByMode(mode));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }


    @Operation(summary = "Delete delivery mode")
    @DeleteMapping("{id}/delete")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204",description = "Delete delivery mode",content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    public ResponseEntity<?>  deleteRole(@PathVariable String id){
        try {
            iDeliveryMode.deleteDeliveryMode(id);
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(null);
        }  catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }
}
