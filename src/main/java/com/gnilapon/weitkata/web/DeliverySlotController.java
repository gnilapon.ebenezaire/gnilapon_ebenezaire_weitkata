package com.gnilapon.weitkata.web;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.DeliverySlotReq;
import com.gnilapon.weitkata.models.dto.responses.ApiErrorResponse;
import com.gnilapon.weitkata.models.dto.responses.DeliverySlotResp;
import com.gnilapon.weitkata.models.enums.Mode;
import com.gnilapon.weitkata.services.IDeliverySlot;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Delivery slot", description = "The delivery slot api")
@RestController
@AllArgsConstructor
@RequestMapping("/delivery-slot")
@CrossOrigin(origins = "*")
@Slf4j
public class DeliverySlotController {

    private IDeliverySlot iDeliverySlot;

    @Operation(
            summary = "Create delivery slot",
            description = "Create delivery slot entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "Save delivery slot",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = DeliverySlotResp.class))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping("/add")
    public ResponseEntity<?> createDeliverySlots(@Valid @RequestBody DeliverySlotReq req) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(iDeliverySlot.createDeliverySlots(req));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }

    @Operation(
            summary = "Create delivery slot",
            description = "Create delivery slot entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Save delivery slot",content = {@Content(mediaType = "application/json",array = @ArraySchema(schema = @Schema(implementation = DeliverySlotResp.class)))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping
    public ResponseEntity<?> createDeliverySlots(@Valid @RequestParam Mode req) {
        try {
            return ResponseEntity.status(HttpStatus.OK).body(iDeliverySlot.getDeliverySlotsByDeliveryMode(req));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }

}
