package com.gnilapon.weitkata.web;

import com.gnilapon.weitkata.exceptions.WeItKataException;
import com.gnilapon.weitkata.models.dto.requests.UserModeReq;
import com.gnilapon.weitkata.models.dto.responses.ApiErrorResponse;
import com.gnilapon.weitkata.services.IUsersMode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Users mode", description = "The delivery mode api")
@RestController
@AllArgsConstructor
@RequestMapping("/users-mode")
@CrossOrigin(origins = "*")
@Slf4j
public class UsersModeController {

    private IUsersMode  iUsersMode;

    @Operation(
            summary = "Create user mode",
            description = "Create user mode entity")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201",description = "Save user mode",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = UserModeReq.class))}),
            @ApiResponse(responseCode = "400",description = "Bad Request",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "404",description = "Not found",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))}),
            @ApiResponse(responseCode = "500",description = "Server error",content = {@Content(mediaType = "application/json",schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @PostMapping("/add")
    public ResponseEntity<?> create(@Valid @RequestBody UserModeReq req) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(iUsersMode.createUserMode(req));
        } catch (final WeItKataException e) {
            log.error(e.getMessage(),e);
            var errorResponse = new ApiErrorResponse();
            errorResponse.setMessage(e.getMessage());
            errorResponse.setStatus(HttpStatus.NOT_FOUND.value());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }
}
