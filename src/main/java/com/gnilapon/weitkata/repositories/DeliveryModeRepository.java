package com.gnilapon.weitkata.repositories;

import com.gnilapon.weitkata.models.entities.DeliveryMode;
import com.gnilapon.weitkata.models.enums.Mode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliveryModeRepository extends JpaRepository<DeliveryMode,String> {
    DeliveryMode findByMode(Mode mode);
}
