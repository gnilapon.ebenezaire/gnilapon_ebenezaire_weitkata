package com.gnilapon.weitkata.repositories;

import com.gnilapon.weitkata.models.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Users,String> {
    Users findByFullNameContaining(String fullName);

    Users findByEmail(String email);
}
