package com.gnilapon.weitkata.repositories;

import com.gnilapon.weitkata.models.entities.DeliverySlot;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeliverySlotRepository extends JpaRepository<DeliverySlot,String> {
}
