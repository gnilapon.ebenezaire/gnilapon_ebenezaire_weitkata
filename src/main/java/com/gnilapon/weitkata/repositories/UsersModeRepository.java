package com.gnilapon.weitkata.repositories;

import com.gnilapon.weitkata.models.entities.UsersMode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersModeRepository extends JpaRepository<UsersMode,String> {
}
