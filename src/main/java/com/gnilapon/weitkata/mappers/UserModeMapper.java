package com.gnilapon.weitkata.mappers;

import com.gnilapon.weitkata.models.dto.requests.UserModeReq;
import com.gnilapon.weitkata.models.dto.responses.UsersModeResp;
import com.gnilapon.weitkata.models.entities.UsersMode;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserModeMapper {
    UserModeMapper INSTANCE = Mappers.getMapper( UserModeMapper.class );
    UsersModeResp toUserModeResp(UsersMode users);
}
