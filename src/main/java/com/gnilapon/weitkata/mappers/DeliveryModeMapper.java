package com.gnilapon.weitkata.mappers;

import com.gnilapon.weitkata.models.dto.requests.DeliveryModeReq;
import com.gnilapon.weitkata.models.dto.responses.DeliveryModeResp;
import com.gnilapon.weitkata.models.entities.DeliveryMode;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DeliveryModeMapper {
    DeliveryModeMapper INSTANCE = Mappers.getMapper( DeliveryModeMapper.class );
    DeliveryMode toDeliveryMode(DeliveryModeReq req);
    DeliveryModeResp toDeliveryModeResp(DeliveryMode deliveryMode);
}
