package com.gnilapon.weitkata.mappers;

import com.gnilapon.weitkata.models.dto.responses.DeliverySlotResp;
import com.gnilapon.weitkata.models.entities.DeliverySlot;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DeliverySlotMapper {
    DeliverySlotMapper INSTANCE = Mappers.getMapper( DeliverySlotMapper.class );
    /*@Mapping(target = "deliveryMode", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createDate", ignore = true)
    @Mapping(target = "updateDate", ignore = true)
    DeliverySlot toDeliverySlot(DeliverySlotReq req);*/
    DeliverySlotResp toDeliverySlotResp(DeliverySlot deliverySlot);
}
