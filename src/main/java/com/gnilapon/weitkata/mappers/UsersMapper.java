package com.gnilapon.weitkata.mappers;

import com.gnilapon.weitkata.models.dto.requests.UsersReq;
import com.gnilapon.weitkata.models.dto.responses.UsersResp;
import com.gnilapon.weitkata.models.entities.Users;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper()
public interface UsersMapper{
    UsersMapper INSTANCE = Mappers.getMapper( UsersMapper.class );
    /*@Mapping(source = "email", target = "email")*/
    Users toUsers(UsersReq req);
    UsersReq toUsersReq(Users users);
   /* @Mapping(target = "usersModes", ignore = true)*/
    UsersResp toUsersResp(Users users);
}
