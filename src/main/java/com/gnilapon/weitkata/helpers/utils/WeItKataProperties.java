package com.gnilapon.weitkata.helpers.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
@Getter
public class WeItKataProperties {
}
