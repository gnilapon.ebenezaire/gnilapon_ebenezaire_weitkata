FROM adoptopenjdk:21-jre

# Create folder in docker
WORKDIR /opt/ms-user

ARG JAR_FILE
COPY ${JAR_FILE} /opt/ms-user/app.jar

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar","/opt/ms-user/app.jar"]