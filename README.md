# Carrefour Java kata -- Delivery

## Exercice
### MVP
#### User Story
> En tant que client, je peux choisir mon mode de livraison.\
> Les modes de livraison disponibles sont : `DRIVE`, `DELIVERY`, `DELIVERY_TODAY`, `DELIVERY_ASAP`.

#### User Story
> En tant que client, je peux choisir mon jour et mon créneau horaire.\
> Les créneaux sont spécifiques au mode de livraison et réservable par d'autres clients.

## Resolution des users stories
- ### Analyse et conception
  - Après decoupage des US, je pense que pour apporter une solution US, les entites suivantes doivent etre implementées : 
    - Utilisateur avec des roles
    - Mode de livraison 
    - les créneaux d'un mode de livraison avec les jours
    - les modes de livraison d'un utilisateur
  - La conception a abouti au diagramme ci-dessous
    
  ![mcd-weitkata](data/mcd-weitkata.png)
  - ### implémentation
      L'implémentation consiste a mettre en l'architecture du projet, notamment en generant le projet sur [spring initializr](https://start.spring.io/) avec Spring-boot 3.2.3, java 21 et en y ajoutant le dependances qui pourraient permettre de repondre aux besoins à l'occurrence HATEOAS, en creant la structure du projet. J'ai implemente les couches persistancces de donnees, la couche DAO , la couche services la couche web. J'ai integre mapstruct pour faciliter la convertion entre entites et DTO et inversement. Integrer swagger pour la documentation des API.
    - ### Reponses a quelques questions bonus
      - #### API REST consommable via http pour interagir avec les services réalisés dans le MVP ,Documentation
      ![endpoint-weitkata](data/endpoint-weitkata.png)
  - #### Persistence
    - Proposer une solution de persistence des données
      
      PostgreSQL:
      ```xml
      <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <scope>runtime</scope>
      </dependency>
      ```
    - Proposer une solution de cache
       
      H2:
     ```xml
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>runtime</scope>
        </dependency>
     ```
### Contraintes
- Spring-boot 3.2.x
- Java 21
- Git : [GitLab](https://gitlab.com/gnilapon.ebenezaire/gnilapon_ebenezaire_weitkata)
### CI/CD
- Un system de CI/CD pour le projet : Jenkinsfile
### Packaging
- Un container de mon application : Dockerfile
- Déploiement mon sur docker : docker-ms-user-dev.yml

```
- For H2
```
spring.datasource.url=jdbc:h2:mem:we-it-kata

spring.datasource.driverClassName=org.h2.Driver

spring.datasource.username=sa

spring.datasource.password=sa

spring.jpa.database-platform=org.hibernate.dialect.H2Dialect

server.port=8090

```
 Comment lancer le projet
```
mvn clean install

mvn spring-boot:run
```
 Swagger documentation
```
http://localhost:8090/we-it-kata/swagger-ui/index.html
```
 H2 management
```
http://localhost:8090/we-it-kata/h2-console
```
